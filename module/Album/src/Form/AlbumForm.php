<?php
namespace Album\Form;

use Zend\Form\Form;

class AlbumForm extends Form
{
    public function __construct($name = null)
    {
        
        parent::__construct('album');

        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);
   
          $this->add([
            'name' => 'message',
            'type' => 'text',
            'options' => [
                'label' => 'message',
            ],
        ]);
           $this->add([
            'name' => 'name',
            'type' => 'text',
            'options' => [
                'label' => 'name',
            ],
        ]); 
         $this->add([
            'name' => 'email',
            'type' => 'text',
            'options' => [
                'label' => 'email',
            ],
        ]);     
        
            $this->add([
            'name' => 'phone',
            'type' => 'text',
            'options' => [
                'label' => 'phone',
            ],
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Go',
                'id'    => 'submitbutton',
            ],
        ]);
    }
}