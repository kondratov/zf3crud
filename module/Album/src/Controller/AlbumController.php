<?php
namespace Album\Controller;

use Album\Model\AlbumTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Album\Form\AlbumForm;
use Album\Model\Album;
use Zend\View\Model\JsonModel;

use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

class AlbumController extends AbstractActionController
{
	private $table;

	public function __construct(AlbumTable $table)
    {
        $this->table = $table;
    }

    public function indexAction()
    {
    	return new ViewModel([
            'albums' => $this->table->fetchAll(),
        ]);
    }

        public function improveAction()
    {
        return new ViewModel([
            'albums' => $this->table->fetchAll(),
        ]);
    }


    public function addAction()
    {
        $form = new AlbumForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();

        if (! $request->isPost()) {
            return ['form' => $form];
        }

        $album = new Album();
        $form->setInputFilter($album->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return ['form' => $form];
        }

        $album->exchangeArray($form->getData());
        $this->table->saveAlbum($album);

        $message = new Message();
        $message->addTo($this->getRequest()->getPost('email', null))
        ->addBcc("zf3.mailer@gmail.com")
        ->addFrom('zf3.mailer@gmail.com')
        ->setSubject('Hello from PSM7!')
        ->setBody($this->getRequest()->getPost('message', null));

$transport = new SmtpTransport();

$options   = new SmtpOptions(array(
   'host'              => 'smtp.gmail.com',
    'port'              => 465, 
    'connection_class'  => 'login',
    'connection_config' => array(
        'username' => 'zf3.mailer@gmail.com',
        'password' => '1234567890987654321',
        'ssl'      => 'ssl',
    ),
));
$transport->setOptions($options);
$transport->send($message);

        return $this->redirect()->toRoute('album');
    }

    public function editAction()
    {
    	$id = (int) $this->params()->fromRoute('id', 0);

        if (0 === $id) {
            return $this->redirect()->toRoute('album', ['action' => 'add']);
        }

       
        try {
            $album = $this->table->getAlbum($id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('album', ['action' => 'index']);
        }

        $form = new AlbumForm();
        $form->bind($album);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        $viewData = ['id' => $id, 'form' => $form];

        if (! $request->isPost()) {
            return $viewData;
        }

        $form->setInputFilter($album->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return $viewData;
        }

        $this->table->saveAlbum($album);
        return $this->redirect()->toRoute('album', ['action' => 'index']);
    }

    public function deleteAction()
    {
    	$id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('album');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->table->deleteAlbum($id);
            }

            return $this->redirect()->toRoute('album');
        }

        return [
            'id'    => $id,
            'album' => $this->table->getAlbum($id),
        ];
    }
}