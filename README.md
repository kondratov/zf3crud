# Инструкция по установке: #
### 1. Клонируем в папку OpenServer\domains командой, указанной ниже: ###

```
#!

git clone https://kondratov@bitbucket.org/kondratov/zf3crud.git
```

### 2. Правим vhost.conf ###

```
#!

<VirtualHost *:%httpport%>
    DocumentRoot    "%hostdir%"
    ServerName      "%host%"
    ServerAlias     "%host%" %aliases%
    ScriptAlias     /cgi-bin/ "%hostdir%/cgi-bin/"
</VirtualHost>
```

### Заменяем на: ###

```
#!

<VirtualHost *:%httpport%>
    DocumentRoot    Локальный/диск/OpenServer/domains/zf3crud/public
    ServerName      "%host%"
    ServerAlias     "%host%" %aliases%
    ScriptAlias     /cgi-bin/ "%hostdir%/cgi-bin/"
</VirtualHost>
```

### 3. Импортируем LIST_FOR_DB.sql в базу. Так как я начал разработку на стандартном драйвере, а не на doctrine, миграция таблицы данных заняла бы много времени для перехода от Zend_Db_Adapter на Doctrine. ###

### 4. В папке zf3crud\config\autoload в файле global.php нужно заменить: ###

### 4.1 db_name на имя вашей БД ###
 
### 4.2 username и password ###

### 5.Страницы доступны по ссылкам: ###
[http://zf3crud/album/improve](Link URL)
[http://zf3crud/album](Link URL)

![1.PNG](https://bitbucket.org/repo/LooAR7k/images/3794361546-1.PNG)
![2.PNG](https://bitbucket.org/repo/LooAR7k/images/828875493-2.PNG)
![3.PNG](https://bitbucket.org/repo/LooAR7k/images/3664113669-3.PNG)
![4.PNG](https://bitbucket.org/repo/LooAR7k/images/425497416-4.PNG)